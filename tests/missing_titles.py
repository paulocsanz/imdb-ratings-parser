# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import csv
import json

import sys
sys.path.append("..")
import scraper

# Parses commandline arguments (--limit and --chunk_size)
parser = argparse.ArgumentParser()
parser.add_argument("file_number", type=int, help="set the number in the filename e.g: info_list_25954.json")
parser.add_argument("-local", action="store_true", help="flag to use local file named with the --file_number e.g: info_list_25954.json")
args = parser.parse_args()

LIMIT = args.file_number
filename = "imdb_ratings_top_%d.csv" % args.file_number

title_year_list = []
if not args.local:
    info_by_id, a = scraper.imdb_ids_scraper(0,LIMIT)
    title_year_list = [(v["title"], v["year"], v["type"]) for k, v in info_by_id.iteritems()]
    
    with open("info_list_%d.json" % LIMIT, "w") as f:
        f.write(json.dumps(title_year_list).encode("utf8"))
else:
    with open("info_list_%d.json" % LIMIT, "r") as f:
        c = json.load(f)
        for i in c:
            title_year_list += [i]


file_title_year_list = []
with open(filename, "rU") as f:
    reader = csv.reader(f)

    for r in reader:
       file_title_year_list += [(r[0], r[1], r[2])]

missing = []

for t, y, _t  in title_year_list:
    if u"(%s, %s, %s)" % (t, y, _t) in file_title_year_list:
        missing += [(t,y, _t)]
if missing:
    print missing
else:
    print "Nothing missing"
