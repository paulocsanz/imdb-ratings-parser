import csv
import sys

# Prints every title in the file and prints how many titles it has
arg = sys.argv[1]

filename = arg
if arg.find(".csv") == -1:
    filename = "imdb_ratings_top_%s.csv" % arg

repeated_obj = []
repeated_elements = []

sys.path.append("..")
with open(filename, "rU") as f:
    reader = csv.reader(f)

    title_names = {}
    repeated_el = {}
    n = 0
    for r in reader:
        t = r[0]
        if t == "Title": continue
        print t + ", " + r[1] + (", " + r[2] if r[2] else "")
        if title_names.get(t):
            repeated_obj += [(t, ((r[1], r[2]), title_names[t]))]
        if repeated_el.get((t, r[1], r[2])):
            repeated_elements += [(t, r[1], r[2])]
        title_names[t] = (r[1], r[2])
        repeated_el[(t, r[1], r[2])] = True
        n += 1
    print "\nRepeated titles:", repeated_obj
    print "Number of repeated titles:", len(repeated_obj)
    print "Repeated elements:", repeated_elements
    print "Number of repeated (title, year, type):", len(repeated_elements)
    print "Total number of titles:", n
