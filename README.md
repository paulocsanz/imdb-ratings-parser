#IMDb ratings HTML scraper.

Creates a CSV file with movie ratings (value and average). Accepts --limit (default: 500) as flag for the number of movies needed and --chunk_size (default: 100) to handle data in chunks and prevent huge buffers when the limit is big and data loss.

To run it needs beautifulsoup4 installed, run (needs pip installed):

    pip install beautifulsoup4

Example on how to run (needs python2 installed):
    
    python scraper.py --limit=500 --chunks=100

That's the default config, so you can just run:

    python scraper.py

CSV Headers:

	Title,Year,Males value,Males average,Females value,Females average,Aged under 18 value,Aged under 18 average,Males under 18 value,Males under 18 average,Females under 18 value,Females under 18 average,Aged 18-29 value,Aged 18-29 average,Males Aged 18-29 value,Males Aged 18-29 average,Females Aged 18-29 value,Females Aged 18-29 average,Aged 30-44 value,Aged 30-44 average,Males Aged 30-44 value,Males Aged 30-44 average,Females Aged 30-44 value,Females Aged 30-44 average,Aged 45+ value,Aged 45+ average,Males Aged 45+ value,Males Aged 45+ average,Females Aged 45+ value,Females Aged 45+ average,IMDb staff value,IMDb staff average,Top 1000 voters value,Top 1000 voters average,US users value,US users average,Non-US users value,Non-US users average
