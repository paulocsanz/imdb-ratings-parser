# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import urllib2
import time
from bs4 import BeautifulSoup

# Parses commandline arguments (--limit and --chunk_size)
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--limit", type=int, help="set the limit of movies to parse - default: 500")
    parser.add_argument("--chunk_size", type=int, help="set the chunk size - default: 100, 0 means no chunks - (script parses ratings in chunks of the limit, to prevent huge buffers and data loss)")
    args = parser.parse_args()
    if args.limit:
        LIMIT = args.limit
    else:
        LIMIT = 500

    if args.chunk_size:
        CHUNK_SIZE = args.chunk_size
    else:
        CHUNK_SIZE = 100

    CSV_KEYS = "Title,Year,Males,Females,Aged under 18,Males under 18,Females under 18,Aged 18-29,Males Aged 18-29,Females Aged 18-29,Aged 30-44,Males Aged 30-44,Females Aged 30-44,Aged 45+,Males Aged 45+,Females Aged 45+,IMDb staff,Top 1000 voters,US users,Non-US users".split(",")

    if CHUNK_SIZE > LIMIT or CHUNK_SIZE == 0:
        CHUNK_SIZE = LIMIT

# Read url data, if it fail waits for "wait" seconds and try again (limit of tries = "_try")
def connect(url, wait=20, _try=10):
    data = ""
    loop = 0

    while loop < _try:
        try:
            data = urllib2.urlopen(url).read()
            loop = _try
        except KeyboardInterrupt:
            raise
        except:
            print "Error connecting to %s, waiting %d seconds before trying again (try: %d of %d)" % (url, wait, loop+1, _try)
            time.sleep(wait)
            loop += 1

    return data

# Parses all IMDb movies' id, title and year
def imdb_ids_scraper(start, limit, info_by_id={}):
    count = 100
    if count > limit: count = limit
    base_url = "http://www.imdb.com/search/title?count=" + str(count) + "&languages=en&num_votes=1000,&sort=num_votes,desc&start=%d&title_type=movies"
    id_list = []

    index = 0
    for i in xrange(1, limit, count):
        print "%d get IMDb movies id (%d to %d) from: " % (index, i+start, i+count+start-1) + base_url % (i+start)
        index +=1
        html = connect(base_url % (i+start))
        title_list = BeautifulSoup(html, "html.parser").body.find_all("div", attrs={"class": "lister-item"})
        for title_obj in title_list:
            id_list += [str(title_obj.find("div", attrs={"class": "ribbonize"})["data-tconst"])]
            h3 = title_obj.find("h3")
            title = h3.find("a").get_text().strip()
            year = h3.find_all("span", attrs={"class":"lister-item-year"})[0].get_text().strip()[1:-1]
            info_by_id[id_list[-1]] = {"title":title, "year": year} 
            if len(id_list) == limit: break
    return info_by_id, id_list

# Parses the ratings from a IMDb movie
def parse_ratings(imdb_id, index):
    base_url = "http://www.imdb.com/title/%s/ratings"

    print "%d get IMDb ratings from: " % index + base_url % imdb_id
    html = connect(base_url % imdb_id)
    table = BeautifulSoup(html, "html.parser").body.find_all("table")[1]
    tr_list = table.find_all("tr")[1:]

    ratings_list = []

    for tr in tr_list:
        td_list = tr.find_all("td")
        if len(td_list) < 2: continue
        if not td_list[0].find("a"): continue
        label = td_list[0].find("a").get_text().strip()
        value = td_list[1].get_text().strip()
        average = td_list[2].find("img").get_text().strip()

        ratings_list += [{"label": label, "value": value, "average": average}]

    return ratings_list

# Calls the ratings parser and format all the data to a sane dict
def get_ratings_and_format_to_dict(id_list, index):
    ratings_dict = {}
    for imdb_id in id_list:
        ratings_dict[imdb_id] = {}
        ratings = parse_ratings(imdb_id, index)
        index += 1
        for r in ratings:
            ratings_dict[imdb_id][r["label"]] = {"value": r["value"], "average": r["average"]}
    return index, ratings_dict

# Formats the ratings to a csv compliant unicode string
def format_to_csv(ratings_dict):
    print "formatting csv"

    csv = ""
    for imdb_id, r in ratings_dict.iteritems():
        csv += u"\"" + info_by_id[imdb_id]["title"] + u"\"," + info_by_id[imdb_id]["year"]
        for k in CSV_KEYS[2:]:
            csv += unicode(property_to_csv(r.get(k) or {"value":"", "average":""}))
        csv += u"\n"
    return csv

# Saves the csv data to file, if it's the first chunk saves the header too
def save_to_file(csv, first, chunk_index):
    file_name = "imdb_ratings_top_%d.csv" % LIMIT
    print "saving chunk %d to file %s" % (chunk_index, file_name)
    
    file_open_mode = "a"
    if first: 
        file_open_mode = "w"
        # Write the first 2 keys in the CSV header (Title, Year) since they only have one value
        CSV_HEADER = unicode(",".join(CSV_KEYS[:2]))
        # Parse the rest of the keys adding the value and the average
        for k in CSV_KEYS[2:]:
            CSV_HEADER += unicode("," + k + " value," + k + " average")
        CSV_HEADER += u"\n"
        csv = CSV_HEADER + csv
    with open(file_name, file_open_mode) as f:
        f.write(csv.encode('utf8'))
    print "saved chunk %d to file %s" % (chunk_index, file_name)

# Expand the dict value and average returning in csv
def property_to_csv(dict):
    return unicode("," + dict["value"] + "," + dict["average"])

if __name__ == '__main__':
    movie_index = 0
    chunk_index = 0
    info_by_id = {}

    # Split LIMIT in chunks to handle huge datasets
    for start in range(0, LIMIT, CHUNK_SIZE):
        if LIMIT - start < CHUNK_SIZE: CHUNK_SIZE = LIMIT - start

        info_by_id, id_list = imdb_ids_scraper(start, CHUNK_SIZE, info_by_id)
        movie_index, ratings_dict = get_ratings_and_format_to_dict(id_list, movie_index)
        csv = format_to_csv(ratings_dict)
        save_to_file(csv, start == 0, chunk_index)

        chunk_index += 1
